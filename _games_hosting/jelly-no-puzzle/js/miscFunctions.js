function fromCoordsToGrid(x, y) { return [floor(x/blockSize), floor(y/blockSize)] }

function fromGridToCoords(n, m) { return [n*blockSize, m*blockSize] }

// This looks more and more like a big blob of spaghetti. I hope it has meatballs in it.
function findObjOnGridCoords(n, m) {
  let result = null
  
  for (let i = 0; i < entities.length; i++) {
    if (entities[i].overlaps(n, m)) {
      result = entities[i];
      break;
    }
  }
  
  return result
}
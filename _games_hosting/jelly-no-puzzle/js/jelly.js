class JellyUnit {
  constructor(n, m, color) {
    this.n = n
    this.m = m
    this.color = color
    this.borders = {top: true,
                    right: true,
                    bottom: true,
                    left: true
                   }
  }
  
  draw(selected) {
    // This is to avoid selected == null. This sets it to false by default.
    selected = selected || false
    
    let temp = fromGridToCoords(this.n, this.m)
    let x = temp[0], y = temp[1]
    
    // Drawing background color
    push()
    noStroke()
    fill(this.color)
    rect(x, y, blockSize)
    pop()
    
    // Drawing borders
    push()
    if (selected) { strokeWeight(4) } else { strokeWeight(2) }
    beginShape(LINES)
    if (this.borders.top) {
      vertex(x, y)
      vertex(x + blockSize, y)
    }
    if (this.borders.right) {
      vertex(x + blockSize, y)
      vertex(x + blockSize, y + blockSize)
    }
    if (this.borders.bottom) {
      vertex(x, y + blockSize)
      vertex(x + blockSize, y + blockSize)
    }
    if (this.borders.left) {
      vertex(x, y)
      vertex(x, y + blockSize)
    }
    endShape()
    pop()
  }
}

class Jelly {
  constructor(n, m, color) {
    this.n = n
    this.m = m
    this.xOffset = 0
    this.yOffset = 0
    this.color = color
    this.jellyUnits = []
    this.selected = false
    
    this.jellyUnits.push(new JellyUnit(0, 0, this.color))
  }
  
  draw() {
    let temp = fromGridToCoords(this.n, this.m)
    let x = temp[0], y = temp[1]
    
    push()

    translate(x + this.xOffset, y + this.yOffset)
    for (let i = 0; i < this.jellyUnits.length; i++) {
      this.jellyUnits[i].draw(this.selected)
    }
    
    pop()
  }
  
  // This function returns a boolean value to the following statement:
  //  "Does this jelly overlap with the given (x, y) coordinates?"
  overlaps(n, m) {
    let result = false
    
    for (let i = 0; i < this.jellyUnits.length; i++) {
      if (this.n + this.jellyUnits[i].n == n && this.m + this.jellyUnits[i].m == m) {
        result = true;
        break;
      }
    }
    
    return result;
  }
  
  // This function returns a boolean value to the following statement:
  //  "Can this jelly move one block to the given direction?"
  // A Jelly can move in a direction when all its Jelly unit can move in that direction
  canMove(direction) {
    let result = true
    let offsetGridCoords
    let objAhead
    
    // Remember: no Jelly can move upwards.
    switch (direction) {
      case "right":
        offsetGridCoords = [1, 0]
        break;
      case "left":
        offsetGridCoords = [-1, 0]
        break;
      case "down":
        offsetGridCoords = [0, 1]
        break;
      default:
        return false;
    }
    
    for (let i = 0; i < this.jellyUnits.length; i++) {
      objAhead = findObjOnGridCoords(
        this.n + this.jellyUnits[i].n + offsetGridCoords[0],
        this.m + this.jellyUnits[i].m + offsetGridCoords[1]
      )
      // If each cell ahead of each JellyUnit in the direction belong to the same jelly or are empty then the whole Jelly can move.
      // Alternatively if exists a cell which does not meet the criteria Jelly can't move.
      if (!(!objAhead || this == objAhead)) {
        result = false;
        break;
      }
    }
    
    return result
  }
  
  // This function returns a boolean value to the following statement:
  //  "Can this jelly fall one block?"
  canFall() {
    return this.canMove("down")
  }
  
  move(direction) {
    let offsetGridCoords
    
    // Remember: no Jelly can move upwards.
    switch (direction) {
      case "right":
        offsetGridCoords = [1, 0]
        break;
      case "left":
        offsetGridCoords = [-1, 0]
        break;
      case "down":
        offsetGridCoords = [0, 1]
    }
  
    this.n += offsetGridCoords[0]
    this.m += offsetGridCoords[1]
  }
  
  // This function returns a boolean value to the following statement:
  //  "Is this jelly adjacent with a given other jelly?"
  //  Two jellys are adjacent when at least one JellyUnit is adjacent to a JellyUnit from the other jelly.
  // Oh boy, here i go n^2 again.
  isAdjacentWith(other) {
    let result = false
    let euclideanDistance
    
    for (let i = 0; i < this.jellyUnits.length; i++) {
      for (let j = 0; j < other.jellyUnits.length; j++) {
        
        // If the Euclidean distance is == 1 then they share an edge.
        euclideanDistance = sqrt(
          pow(this.n + this.jellyUnits[i].n - (other.n + other.jellyUnits[j].n), 2) +
          pow(this.m + this.jellyUnits[i].m - (other.m + other.jellyUnits[j].m), 2)
        )
        if (euclideanDistance == 1) {
          result = true;
          break;
        }
      }
    }
    
    return result
  }
  
  // This function copies all JellyUnit from other to self. It's up to the caller to delete other.
  merge(other) {
    for (let i = 0; i < this.jellyUnits.length; i++) {
      for (let j = 0; j < other.jellyUnits.length; j++) {
        
        // Checking top adjacency from "this" perspective
        if (this.n + this.jellyUnits[i].n == other.n + other.jellyUnits[j].n &&
            this.m + this.jellyUnits[i].m - 1 == other.m + other.jellyUnits[j].m) {
          this.jellyUnits[i].borders.top = false
          other.jellyUnits[j].borders.bottom = false
        }
        
        // Checking bottom adjacency from "this" perspective
        if (this.n + this.jellyUnits[i].n == other.n + other.jellyUnits[j].n &&
            this.m + this.jellyUnits[i].m + 1 == other.m + other.jellyUnits[j].m) {
          this.jellyUnits[i].borders.bottom = false
          other.jellyUnits[j].borders.top = false
        }
        
        // Checking left adjacency from "this" perspective
        if (this.n + this.jellyUnits[i].n - 1 == other.n + other.jellyUnits[j].n &&
            this.m + this.jellyUnits[i].m == other.m + other.jellyUnits[j].m) {
          this.jellyUnits[i].borders.left = false
          other.jellyUnits[j].borders.right = false
        }
        
        // Checking right adjacency from "this" perspective
        if (this.n + this.jellyUnits[i].n + 1 == other.n + other.jellyUnits[j].n &&
            this.m + this.jellyUnits[i].m == other.m + other.jellyUnits[j].m) {
          this.jellyUnits[i].borders.right = false
          other.jellyUnits[j].borders.left = false
        }
      }
    }
    
    for (let i = 0; i < other.jellyUnits.length; i++) {
      other.jellyUnits[i].n += other.n - this.n
      other.jellyUnits[i].m += other.m - this.m
      this.jellyUnits.push(other.jellyUnits[i])
    }
  }
}

class WallUnit {
  constructor(n, m) {
    this.n = n
    this.m = m
  }
  
  draw() {
    let temp = fromGridToCoords(this.n, this.m)
    let x = temp[0], y = temp[1]
    
    push()
    
    translate(x, y)
    noStroke()
    fill(128+16)
    rect(0, 0, blockSize)
    
    pop()
  }
  
  overlaps(n, m) {
    return this.n == n && this.m == m
  }
}
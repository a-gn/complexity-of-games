function setup() {
  let cnv = createCanvas(gridWidth * blockSize, gridHeight * blockSize)
  cnv.parent("myCanvasContainer")
  noSmooth()
  entities = []
  timeSincePressed = 0
  animation = {currentlyAnimating: false,
              jelly: null,
              direction: null}
}

function draw() {
  background(255);
  
  // Here WallUnit(s) are supposed to be in front of the array so they get drawn first.
  //  The sorting happens in generateLevel().
  for (let i = 0; i < entities.length; i++) {
    if (animation.currentlyAnimating && animation.jelly === entities[i]) {
      let offsetDirection
      switch (animation.direction) {
        case "left":
          offsetDirection = [-1, 0];
          break;
        case "right":
          offsetDirection = [1, 0];
          break;
        case "down":
          offsetDirection = [0, 1];
      }
      
      // To calculate distance we do time elapsed times velocity of animation
      let deltaX = (deltaTime * pow(10, -3)) * blockSize/animationTime * offsetDirection[0]
      let deltaY = (deltaTime * pow(10, -3)) * blockSize/animationTime * offsetDirection[1]
      
      animation.jelly.xOffset += deltaX
      animation.jelly.yOffset += deltaY
      
      if (abs(animation.jelly.xOffset) > blockSize ||
          abs(animation.jelly.yOffset) > blockSize) {
        animation.jelly.xOffset = 0
        animation.jelly.yOffset = 0
        animation.jelly.move(animation.direction)
        
        animation.currentlyAnimating = false
        animation.jelly = null
        animation.direction = null
        
        gravityMergeWinTurn()
      }
    }
    entities[i].draw()
  }
  
  // This code means: if the a key is held down keyTyped() is called at this incremental intervals
  // instantly, 600 ms, 100 ms, 100 ms, 100 ms, 100 ms, ...
  if (keyIsPressed) {
    timeSincePressed += deltaTime
    if (timeSincePressed > 100 + 500) {
      keyTyped()
      timeSincePressed = 500
    }
  } else {
    timeSincePressed = 0
  }
}

function gravityTurn() {
  for (let i = 0; i < entities.length; i++) {
    if (entities[i].constructor.name == "Jelly" && entities[i].canFall()) {
      animation.currentlyAnimating = true;
      animation.jelly = entities[i];
      animation.direction = "down"
    }
  }
}

function mergeTurn() {
  // When we merge we remove the object leaving a null in the place of the absorbed object
  //  We then filter all the nulls out of the array. Other function except every element to be something.
  for (let i = entities.length - 1; i >= 0; i--) {
    for (let j = entities.length - 1; j >= 0; j--) {
      if (i != j && entities[i] && entities[j] &&
          entities[i].constructor.name == "Jelly" &&
          entities[j].constructor.name == "Jelly" &&
          JSON.stringify(entities[i].color) == JSON.stringify(entities[j].color)) {
        if (entities[i].isAdjacentWith(entities[j])) {
          // If one of the Jelly is selected then the whole merged Jelly is selected 
          entities[i].selected = entities[i].selected || entities[j].selected
          entities[i].merge(entities[j])
          delete entities[j]
        }
      }
    }
  }
  
  entities = entities.filter(
    function (el) {
      return el != null;
    }
  )
}

function winCondition() {
  let result = true
  let count = {}
  for (let i = 0; i < entities.length; i++) {
    if (entities[i].constructor.name == "Jelly") {
      if (count[JSON.stringify(entities[i].color)] != null) {
        count[JSON.stringify(entities[i].color)] += 1
      } else {
        count[JSON.stringify(entities[i].color)] = 1
      }
    }
  }
  
  for (const [key, value] of Object.entries(count)) {
    if (value != 1) {
      result = false;
      break;
    }
  }

  return result
}

function gravityMergeWinTurn (){
  gravityTurn()
  
  mergeTurn()
  
  if (winCondition()) {
    redraw()
    setTimeout(() => alert("Level completed!"), 250)
  }
}

function mouseClicked() {
  let temp = fromCoordsToGrid(mouseX, mouseY)
  let n = temp[0], m = temp[1]
  let objFound = findObjOnGridCoords(n, m)
  
  if (animation.currentlyAnimating) { return; }
  
  for (let i = 0; i < entities.length; i++) {
    if (entities[i].constructor.name == "Jelly") {
      entities[i].selected = false
    }
  }
  
  if (objFound && objFound.constructor.name == "Jelly") {
    objFound.selected = true
  }
}

function keyTyped() {
  let direction, selected
  
  if (animation.currentlyAnimating) { return; }
  
  // Movement turn.
  switch (key) {
    case "a":
      direction = "left";
      break;
    case "d":
      direction = "right";
      break;
    default:
      return true;
  }
  
  for (let i = 0; i < entities.length; i++) {
    if (entities[i].selected) {
      selected = entities[i];
      break;
    }
  }
  
  if (selected && selected.canMove(direction)) {
    animation.currentlyAnimating = true;
    animation.jelly = selected;
    animation.direction = direction
  }
  
  // P5 refernce tells you to do this. No idea why.
  return false
}

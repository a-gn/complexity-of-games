function generateLevel() {
  let instanceInput = ThreePartitionInput.value
  
  // Check if the list contains only positive integers.
  instanceInput.replace(/\s+/g, '')
  instanceInput = instanceInput.split(/[ ,]+/)
  for (let i = 0; i < instanceInput.length; i++) {
    instanceInput[i] = Number(instanceInput[i])
    
    if (!(typeof instanceInput[i] == "number" && Number.isInteger(instanceInput[i]))) {
      alert("One of the entries was not an integer")
      return
    }
      
    if (instanceInput[i] <= 0) {
      alert("One of the entries was not a positive integer")
      return
    }
  }
  
  // Check if the number of elements is a multiple of 3.
  if (instanceInput.length % 3 != 0) {
    alert("The number of entries is not a multiple of 3")
    return
  }
  let m = instanceInput.length / 3
  
  // Check if the sum of all elements is a multiple of m.
  let sum = 0
  for (let i = 0; i < instanceInput.length; i++) {
    sum += instanceInput[i]
  }
  if (sum % m != 0) {
    alert("The sum of all entries is not a multiple of the number of partitions")
    return
  }
  let B = sum / m
    
  // Check if every element in the list is between B/4 and B/2
  for (let i = 0; i < instanceInput[i]; i++) {
    if (!(instanceInput[i] > B/4 && instanceInput[i] < B/2)) {
      alert("One of the entries is not in the right range of values")
      return
    }
  }
  
  // Create level of Jelly-No Puzzle
    
  // Calculating new width and height of the level
  let newWidth = 0, newHeight = 0
  let initialPlatformWidth, partitionsPlatformWidth
  
  newWidth += 2  // Walls
  newWidth += 2  // Offset spaces on left and right
  partitionsPlatformWidth = (1 + B) * m + 3
  newWidth += partitionsPlatformWidth
  initialPlatformWidth = 2 + sum + (instanceInput.length - 1)
  newWidth += initialPlatformWidth
  newWidth += -2  // Because initial platform and partitions platform overlap
  
  newHeight += 2  // Walls
  newHeight += 2  // Offset spaces on top and bottom
  newHeight += 2  // Top platform with inital Jelly
  newHeight += 2 * (m - 1)  // There are m - 1 platform for descending of height 2
  newHeight += 4  // Every partition has the final little platforms for entering
  newHeight += 3  // Partitions platform
  
  gridWidth = newWidth
  gridHeight = newHeight
  
  setup()
    
  // Border walls
  for (let i = 0; i < gridWidth; i++) {
    for (let j = 0; j < gridHeight; j++) {
      if (i == 0 || i == gridWidth - 1 || j == 0 || j == gridHeight - 1) {
        entities.push(new WallUnit(i, j))
      }
    }
  }
  
  // Initial Jelly platform
  for (let i = gridWidth - initialPlatformWidth - 2; i < gridWidth - 2; i++) {
    entities.push(new WallUnit(i, 3))
  }
  for (let i = 0, n = gridWidth - initialPlatformWidth - 1, tempJelly; i < instanceInput.length; n += instanceInput[i] + 1, i++) {
    tempJelly = new Jelly(n, 2, [255, 64+32, 64+32])
    if (instanceInput[i] > 1) { tempJelly.jellyUnits[0].borders.right = false }
    for (let j = 1, tempJellyUnit; j < instanceInput[i]; j++) {
      tempJellyUnit = new JellyUnit(j, 0, tempJelly.color)
      tempJellyUnit.borders.left = false
      if (j != instanceInput[i] - 1) { tempJellyUnit.borders.right = false }
      tempJelly.jellyUnits.push(tempJellyUnit)
    }
    entities.push(tempJelly)
  }
  
  // Partitions platform
  entities.push(new WallUnit(2, gridHeight - 4))
  for (let i = 0, tempJelly; i < m; i++) {
    for (let j = 0; j < B + 1; j++) {
      if (j == 0) {
        tempJelly = new Jelly(3 + i * (B + 1), gridHeight - 5, [255, 64+32, 64+32])
        tempJelly.jellyUnits.push(new JellyUnit(0, 1, tempJelly.color))
        tempJelly.jellyUnits[0].borders.bottom = false
        tempJelly.jellyUnits[1].borders.top = false
        entities.push(tempJelly)
      } else {
        entities.push(new WallUnit(3 + j + i * (B + 1), gridHeight - 4))
      }
    }
  }
  let tempJelly = new Jelly(partitionsPlatformWidth, gridHeight - 5, [255, 64+32, 64+32])
  tempJelly.jellyUnits.push(new JellyUnit(0, 1, tempJelly.color))
  tempJelly.jellyUnits[0].borders.bottom = false
  tempJelly.jellyUnits[1].borders.top = false
  entities.push(tempJelly)
  entities.push(new WallUnit(partitionsPlatformWidth + 1, gridHeight - 4))
  for (let i = 2; i < partitionsPlatformWidth + 2; i++) {
    entities.push(new WallUnit(i, gridHeight - 3))
  }
  
  // 2x1 walls to descend into partition
  for (let i = 0, n; i < m; i++) {
    n = 3 + (i + 1) * (B + 1)
    entities.push(new WallUnit(n, gridHeight - 6))
    entities.push(new WallUnit(n + 1, gridHeight - 6))
    entities.push(new WallUnit(n, gridHeight - 8))
    entities.push(new WallUnit(n - 1, gridHeight - 8))
  }
  
  // Descending Bx1 platforms. We need m - 1 of these.
  for (let i = 1, n, k; i < m; i++) {
    n = 3 + (i) * (B + 1)
    k = gridHeight - 8 - 2 * (i)
    for (let j = n; j < n + B + 1; j++) {
      entities.push(new WallUnit(j, k))
    }
  }
  
  /* We sort entities making sure that all the walls are before all the jellies */
  entities.sort(
    function (a, b) {
      if (a.constructor.name == b.constructor.name) {
        return 0
      } else if (a.constructor.name == "WallUnit") {
        return -1
      } else if (a.constructor.name == "Jelly") {
        return 1
      } else {
        console.log("This line isn't really supposed to run")
        return 0
      }
    }
  )

  document.getElementById("myCanvasContainer").style.display="initial";
  redraw()
}

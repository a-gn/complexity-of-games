"use strict";

//drawing parameters
const PEG_DIAMETER = 10,
      PEG_RADIUS = PEG_DIAMETER / 2,
      PEG_MARGIN = Math.max(2, PEG_DIAMETER/4),
      TILE_SIZE = PEG_DIAMETER + PEG_MARGIN,
      PEG_COLOR = "black",
      PEG_HOLE_COLOR = "lightgray",
      PEG_SELECTED_COLOR = "red",
      BACKGROUND_COLOR = "white";

//utility functions
//adapted from https://stackoverflow.com/a/18053642/12348372
function getCanvasClickPosition(canvas, event) {
    const rect = canvas.getBoundingClientRect();
    const x = event.clientX - rect.left;
    const y = event.clientY - rect.top;
    return [x,y];
}

//return a callback function that
//- checks if a peg was clicked
//- calculate the clicked peg's coordinates
//- calls the pegClickedCallback of a board with these coordinates
function getCanvasClickCallback(board) {
    return function(event) {
        //did we click on a peg, and if so, which one?
        //first check in which tile we are
        const [x,y] = getCanvasClickPosition(this, event);
        const row = Math.floor(y / TILE_SIZE),
                col = Math.floor(x / TILE_SIZE);
        //calculate the peg's center's coordinates
        const centerX = TILE_SIZE * col + PEG_MARGIN + PEG_RADIUS,
              centerY = TILE_SIZE * row + PEG_MARGIN + PEG_RADIUS;
        const centerOffX = x - centerX,
              centerOffY = y - centerY;
        //are we in the circle?
        if (Math.sqrt((centerOffX*centerOffX) + (centerOffY*centerOffY)) <= PEG_RADIUS) {
            //yes we are
            board.pegClickedCallback(col, row);
        }
    };
}

//return a callback function that calls a PlayingBoard's pegClickedCallback
function getPegClickCallback(playingBoard) {
    return function(col, row) {
        playingBoard.pegClickedCallback(col, row);
    }
}

//game code

//a game board where pegs can be added, removed and selected at will
class Board {
    constructor(canvasID, boardState, pegClickedCallback) {
        //store properties
        this.width = boardState.length;
        this.height = boardState[0].length;

        //update canvas size
        this.canvas = document.getElementById(canvasID);
        this.canvas.width = this.width * (PEG_DIAMETER + PEG_MARGIN) + PEG_MARGIN
        this.canvas.height = this.height * (PEG_DIAMETER + PEG_MARGIN) + PEG_MARGIN

        //listen to canvas events
        this.pegClickedCallback = pegClickedCallback;
        const canvasClickedCallback = getCanvasClickCallback(this);
        this.canvas.addEventListener("click", canvasClickedCallback);

        //prevent text selection on double click (https://stackoverflow.com/a/3685462/12348372)
        this.canvas.onselectstart = function () { return false; }

        //get canvas context and clear the board
        this.ctx = this.canvas.getContext("2d");
        this.ctx.fillStyle = BACKGROUND_COLOR;
        this.ctx.fillRect(0, 0, TILE_SIZE*this.width, TILE_SIZE*this.height);

        //initialize board state
        this.state = new Array(this.width).fill(null).map(() => new Array(this.height));
        //we were given a full initial state, copy it
        for (let x = 0; x < this.width; x++) {
            for (let y = 0; y < this.height; y++) {
                if (boardState[x][y]) {
                    this.setPeg(x, y);
                } else {
                    this.removePeg(x, y);
                }
            }
        }

        //initialize selected peg
        this.selectedPeg = {
            selected: false,
            col: -1,
            row: -1
        }
    }

    drawPeg(col, row, fillStyle) {

        let yc = PEG_DIAMETER * (row + 1 / 2) + PEG_MARGIN * (row + 1),
            xc = PEG_DIAMETER * (col + 1 / 2) + PEG_MARGIN * (col + 1);

        //redraw the whole tile to avoid artifacts
        this.ctx.fillStyle = BACKGROUND_COLOR;
        this.ctx.fillRect(xc-TILE_SIZE/2, yc-TILE_SIZE/2, TILE_SIZE, TILE_SIZE);

        this.ctx.beginPath();
        this.ctx.arc(xc, yc, PEG_RADIUS, 0, 2 * Math.PI);
        this.ctx.fillStyle = fillStyle;
        this.ctx.fill();
    }

    setPeg(col, row) {
        this.state[col][row] = true;
        this.drawPeg(col, row, PEG_COLOR);
    }

    removePeg(col, row) {
        this.state[col][row] = false;
        this.drawPeg(col, row, PEG_HOLE_COLOR);
    }

    isCellFull(col, row) {
        return this.state[col][row];
    }

    attemptSelectPeg(col, row) {
        if (this.isCellFull(col, row)) {
            this.selectedPeg.selected = true;
            this.selectedPeg.col = col;
            this.selectedPeg.row = row;
            this.drawPeg(col, row, PEG_SELECTED_COLOR);
            return true;
        } else {
            return false;
        }
    }

    attemptUnselectPeg() {
        if (this.selectedPeg.selected) {
            this.selectedPeg.selected = false;
            this.drawPeg(this.selectedPeg.col, this.selectedPeg.row, PEG_COLOR);
            return true;
        } else {
            return false;
        }
    }

    hasState(boardState) {
        for (let x = 0; x < this.state.length; x++) {
            for (let y = 0; y < this.state[x].length; y++) {
                if (this.state[x][y] != boardState[x][y]) {
                    return false;
                }
            }
        }
        return true;
    }

    legalMoveExists() {
        for (let x = 0; x < this.width; x++) {
            for (let y = 0; y < this.height; y++) {
                for (var [dx, dy] of [ [1,0], [-1, 0], [0, 1], [0, -1] ] ) {
                    if ( !(x + 2*dx >=0 && x + 2*dx < this.width &&  y + 2*dy >= 0 && y + 2*dy <= this.height) )
                        continue;
                    
                    if(this.state[x][y] && this.state[x+dx][y+dy] && !this.state[x+2*dx][y+2*dy])
                        return true;
                }
            }
        }
        return false;
    }
}

//a game board that follows game logic
class PlayingBoard {
    constructor(canvasID, initialState, targetState, gameOverCallback, allowClicks=true) {
        this.targetState = targetState;
        this.allowClicks = allowClicks;
        this.gameOverCallback = gameOverCallback;
        this.board = new Board(canvasID, initialState, getPegClickCallback(this));
        this.checkGameOver(); //in case the initial state is winning/losing
    }

    pegClickedCallback(col, row) {
        if (!(this.allowClicks)) {
            //this board doesn't accept click controls (only method calls)
            return;
        }
        if (this.board.selectedPeg.selected) {
            this.board.attemptUnselectPeg();
            let moved = this.attemptMove(this.board.selectedPeg.col, this.board.selectedPeg.row, col, row);
            if (moved) {
                this.checkGameOver();
            }
        } else {
            this.board.attemptSelectPeg(col, row);
        }
    }

    checkGameOver() {
        if (this.gameWon()) {
            this.gameOverCallback(true);
        } else if (!this.board.legalMoveExists()) {
            this.gameOverCallback(false);
        }
    }

    attemptMove(oldCol, oldRow, newCol, newRow) {
        const xMov = newCol - oldCol;
        const yMov = newRow - oldRow;
    
        //only horizontal moves
        if (xMov != 0 && yMov != 0) {
            return false;
        }
        //must move two tiles
        if (Math.abs(xMov+yMov) != 2) {
            return false;
        }
        //does the peg exist, and is there room at the target position?
        if (!this.board.isCellFull(oldCol, oldRow) || this.board.isCellFull(newCol, newRow)) {
            return false;
        }
        //is there a peg to jump over?
        const jumpedCol = (newCol+oldCol)/2;
        const jumpedRow = (newRow+oldRow)/2;
        if (!this.board.isCellFull(jumpedCol, jumpedRow)) {
            return false;
        }
        //we can move
        this.board.removePeg(oldCol, oldRow);
        this.board.setPeg(newCol, newRow);
        this.board.removePeg(jumpedCol, jumpedRow);
        return true;
    }

    gameWon() {
        return this.board.hasState(this.targetState);
    }
}

//an object that plays automatically to show a level's solution
class AutoPlayer {
    constructor(board, moveList) {
        this.board = board;
        this.moveList = moveList;
        this.stepIndex = 0;
        this.endStep = moveList.length;
    }

    step() {
        if (this.stepIndex == this.endStep) {
            return false;
        } else {
            const m = this.moveList[this.stepIndex];
            const r = this.board.attemptMove(m[0], m[1], m[2], m[3]);
            if (!r) {
                throw new Error("bad move in AutoPlayer");
            }
            this.stepIndex++;
        }
    }
}

function parseBoardState(stringState) {
    //get state size
    const lines = stringState.split("\n");
    const lineLen = lines[0].length;
    const rowLen = lines[0].length + 2;
    const colLen = lines.length + 2;

    //check that row size is consistent
    for (let y = 0; y < lines.length; y++) {
        if (lines[y].length != lineLen) {
            throw new Error(`bad board state: line ${y+1} has length ${lines[y].length}, expected ${lineLen}`);
        }
    }

    //allocate the state array
    const state = new Array(rowLen);
    for (let x = 0; x < rowLen; x++) {
        state[x] = new Array(colLen);
    }

    //parse the state
    for (let x = 1; x < rowLen - 1; x++) {
        for (let y = 1; y < colLen - 1; y++) {
            const c = lines[y-1][x-1];
            switch (c) {
                case "O":
                    state[x][y] = true;
                    break;
                case "X":
                    state[x][y] = false;
                    break;
                default:
                    throw new Error(
                        `bad character at line ${y}, column ${x} of state ('${c}' instead of 'O' or 'X')`
                    );
            }
        }
    }

    //add borders of one tile
    for (let x = 0; x < rowLen; x++) {
        state[x][0] = false;
        state[x][colLen - 1] = false;
    }
    for (let y = 0; y < colLen; y++) {
        state[0][y] = false;
        state[rowLen - 1][y] = false;
    }

    return state;
}

function parseMoveList(stringState) {
    //get move count
    const lines = stringState.split("\n");
    const moveCount = lines.length;

    //allocate the move array
    const moves = new Array(moveCount);
    for (let x = 0; x < moveCount; x++) {
        moves[x] = new Array(4);
    }

    //parse the moves
    for (let x = 0; x < moveCount; x++) {
        const tokens = lines[x].split(" ");
        if (tokens.length != 4) {
            throw new Error(
                `parsing error on line ${x+1}: line contains ${tokens.length} tokens, expected 4`
            );
        }
        for (let y = 0; y < 4; y++) {
            const coord = parseInt(tokens[y]);
            if (isNaN(coord)) {
                throw new Error(
                    `parsing error on line ${x+1}: could not parse token '${tokens[y]}' (integer expected)`
                );
            }
            moves[x][y] = coord;
        }
    }

    return moves;
}

//game instance
const INITIAL_STATE = `XXXXXXXXXXXXXXXOXXXXXXXXXXXXXX
XXXXXXXXXXXXOXOXOXOXOXXXXXXXXX
XXXXXXXXXXXOXXXXXXXXXOXXXXXXXX
XXXXXXXXXXOXXXXXXXXXXXOXXXXXXX
XXXXXXXXXOXXXXXOXXXXXXXOXXXXXX
XXXXXXXXOXXXXXXXXOXXXXXXOXXXXX
XXXXXXXOXXXXXXXXOXXXXXXXXOXXXX
XXXXXXOXXXXXXXOXXXXXXXXXXXXXXX
XXXXXOXXXXXXXXXOXXXXXXXXXOXXXX
XXXXOXXXXXXXXXXXXXXXXXXXXXXXXX
XXXOXXXXXXXXXXXOXXXXXXXXXOXXXX
XXOXXXXXXXXXXXXOXXXXXXXXXXXXXX
XOXXXXXXXXXXXXXOXXXXXXXXXOXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XOXXXXXXXXXXXXXOXXXXXXXXXOXXXX
XXXXXXXXXXXXXXXXXOXXXXXXXXOXXX
XOXXXXXXXXXXXXXXOXXXXXXXOXXXXX
XXXXXXXXXXXXXXXXXXXXXXXOXXXXXX
XOXXXXXXXXXXXXXXOXXXXXXXXOXXXX
XXXXXXXXXXXXXXOXXXOXXXXXXXXXXX
XOXOXXXXXXXXXXXXXXXXXXXXXXXXXX
OOXXXXXXOXOXOXOOXOOXOXOXOXOXOX
OXXXOXOXXXXXXXXOOOXXXXXXXOXXXO
OOXXXXXOXXXXXXXXXXXXXXXXXXXXXX
XOXOXXXXXXXXXXXXXXXXXXXXXXXXXO
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XOXXXXXXXXXXXXXXXXXXXXXXXXXXXO
XXOXOXOXOXOXOXOXOXOXOXOXOXOXOX`;

const TARGET_STATE = `XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXOXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX`;

const SOLUTION = `17 23 19 23
19 23 19 21
19 20 19 22
18 22 20 22
20 22 22 22
22 22 24 22
24 22 26 22
1 23 1 21
1 21 3 21
4 21 2 21
2 22 2 20
2 20 2 18
2 18 2 16
2 16 2 14
2 14 2 12
2 12 4 12
4 12 4 10
4 10 6 10
6 10 6 8
6 8 8 8
8 8 8 6
8 6 10 6
10 6 10 4
10 4 12 4
12 4 12 2
12 2 14 2
14 2 16 2
16 12 16 10
16 10 16 8
15 8 17 8
17 8 17 6
18 6 16 6
16 6 16 4
16 1 16 3
16 4 16 2
16 2 18 2
18 2 20 2
20 2 22 2
22 2 22 4
22 4 24 4
24 4 24 6
24 6 26 6
26 6 26 8
26 8 26 10
26 10 26 12
26 12 26 14
26 14 26 16
27 16 25 16
25 16 25 18
24 18 26 18
26 18 26 20
26 23 26 21
26 20 26 22
26 22 28 22
28 22 30 22
30 22 30 24
30 24 30 26
30 26 30 28
30 28 28 28
28 28 26 28
26 28 24 28
24 28 22 28
22 28 20 28
20 28 18 28
18 28 16 28
16 28 14 28
14 28 12 28
12 28 10 28
10 28 8 28
8 28 6 28
6 28 4 28
4 28 2 28
2 28 2 26
1 24 3 24
2 26 2 24
2 24 4 24
4 25 4 23
4 23 6 23
6 23 8 23
8 24 8 22
8 22 10 22
10 22 12 22
12 22 14 22
16 23 16 21
14 22 16 22
16 22 16 20
15 20 17 20
17 20 17 18
17 18 17 16
18 16 16 16
16 16 16 14
16 14 16 12`;

const initialState = parseBoardState(INITIAL_STATE);
const targetState = parseBoardState(TARGET_STATE);
const solution = parseMoveList(SOLUTION);

//create the playing board
const playBoard = new PlayingBoard(
    "playboard",
    initialState,
    targetState,
    function(won) {
        if (won) {
            alert("you won! :D");
        } else {
            alert("you lost :(");
        }
    },
    true
);

//create the board showing the target state
const targetBoard = new Board(
    "targetboard",
    targetState,
    function() {}
);

//create the board showing the solution and the AutoPlayer managing it
const solutionBoard = new PlayingBoard(
    "solutionboard",
    initialState,
    targetState,
    function(_) { },
    false
);
const solutionAutoPlayer = new AutoPlayer(solutionBoard, solution);

//set up the button advancing the solution
const stepButton = document.getElementById("auto-step-button");
stepButton.addEventListener(
    "click",
    function() {
        solutionAutoPlayer.step();
    }
);
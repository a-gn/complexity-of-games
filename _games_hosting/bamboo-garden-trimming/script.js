class Bamboo {
    constructor(tag, rate) {
        this.tag = tag;
        this.rate = rate;
    }

    query() {
        return this;
    }
}


class Garden {
    constructor(rates, elementName)
    {
        this.rates = rates;
        this.H = this.rates.reduce((total, current) => total + current, 0);

        this.bamboos = [];
        this.heights = Array(this.rates.length).fill(0);
        for (var i = 0; i < this.rates.length; i++)
            this.bamboos.push(new Bamboo(i, this.rates[i]));


        var canvas = document.getElementById(elementName);
        this.width = this.bamboos.length * 50 + 50; //50px margin the left side on each side
        this.height = 300;

        canvas.style.width  = this.width.toString() + "px";
        canvas.style.height = this.height.toString() + "px";
        var dpr = window.devicePixelRatio || 1;
        canvas.width = this.width * dpr;
        canvas.height = this.height * dpr;
        this.ctx = canvas.getContext("2d");
        this.ctx.scale(dpr, dpr);
        
        this.max_height = 2.5*this.H;
        
        this.panda_loaded = false;
        this.panda = new Image();
        this.panda.onload = () => { this.panda_loaded = true; };
        this.panda.src = "images/panda-small.png";
        
        this.last_cut = -1;
        this.makespan = 0;
        
        //this.ctx.imageSmoothingEnabled = false;
    }

    cut(idx)
    {   
        this.last_cut = idx;

        if(idx!=null)
            this.heights[idx] = 0;
    }

    grow() {
        for(var i = 0; i < this.bamboos.length; i++)
            this.heights[i] += this.bamboos[i].rate;
        
        this.makespan = Math.max(this.makespan, ...this.heights)
        this.max_height = Math.max(this.max_height, this.makespan + this.H/10);
    }

    clear() {
        this.ctx.clearRect(0, 0, this.width,this.height);
        this.ctx.beginPath();
    }

    HLineAt(h, color) {
        h = Math.round(this.height * h / this.max_height);

        this.ctx.strokeStyle = color;
        this.ctx.beginPath();
        this.ctx.moveTo(0, this.height - h);
        this.ctx.lineTo(this.width, this.height - h);
        this.ctx.stroke();
    }

    draw() {
        this.clear();
        
        var grd = this.ctx.createLinearGradient(0, 0, 0, this.height);
        grd.addColorStop(0, "#88CCFF");
        grd.addColorStop(0.8, "#CCFFFF");
        grd.addColorStop(0.85, "#CCFFCC");
        grd.addColorStop(0.98, "#BBFFBB");
        grd.addColorStop(1, "#999966");
        this.ctx.fillStyle = grd;
        this.ctx.fillRect(0, 0, this.width, this.height);

        
        this.ctx.font = "12px sans-serif";
        this.ctx.textAlign = "left";
        for(h=this.H; h<=this.max_height; h+=this.H)
        {
            this.HLineAt(h, "#666666");
            this.ctx.strokeStyle = this.ctx.fillStyle = "#000000";
            this.ctx.fillText(h.toString(), 0, this.height - this.height * h / this.max_height - 1);
        }
        
        this.HLineAt(this.makespan, "#FF6666");
        this.ctx.textAlign = "right";
        this.ctx.strokeStyle = this.ctx.fillStyle  = "#FF3333";
        this.ctx.fillText(this.makespan.toString(), this.width, this.height - this.height * this.makespan / this.max_height - 1);
                

        for (var i = 0; i < this.heights.length; i++)
        {
            var grd = this.ctx.createLinearGradient(i*50 + 50, 0, i*50 + 50 + 6, 0);
            grd.addColorStop(0, "#006600");
            grd.addColorStop(0.5, "#008000");
            grd.addColorStop(1, "#006600");
            this.ctx.fillStyle = grd;
            
            var h = this.heights[i] * this.height / this.max_height;
            this.ctx.fillRect(i*50 + 50, this.height - h + 1, 6, h);

            this.ctx.beginPath();
            this.ctx.ellipse(i*50+50+3, this.height - h + 2, 3, 2.5, 0, Math.PI, 2*Math.PI);
            this.ctx.fill();

            for(var knot=this.rates[i]; knot<this.heights[i]; knot+=this.rates[i])
            {
                var h = Math.round(knot * this.height / this.max_height);
                
                grd = this.ctx.createRadialGradient(i*50+50+3, this.height - h, 0, i*50+50+3, this.height - h, 4);
                grd.addColorStop(0, "#006600");
                grd.addColorStop(0.5, "#008000");
                grd.addColorStop(1, "#006600");
                this.ctx.fillStyle = grd;
                                            
                this.ctx.beginPath();
                this.ctx.arc(i*50+50+3, this.height - h, 4, 0, 2 * Math.PI);
                this.ctx.fill();
            }
            
        }
        

        if(this.last_cut != null && this.panda_loaded)
            this.ctx.drawImage(this.panda, this.last_cut*50 + 50 + 8, this.height - 40, 40, 40);            
    }
}

function build() {
    var split = document.getElementById("input").value.split(" ");
    var rates = [];
    for (var i = 0; i < split.length; i++) {
        var r = parseInt(split[i]);
        if (isNaN(r) || r <= 0)
            continue;

        rates.push(r);
    }

    var parsed = document.getElementById("parsed");
    parsed.innerHTML = "";
    for (var i = 0; i < rates.length; i++)
        parsed.innerHTML += (((i!=0)?", ":"") + "b" + (i+1) + ": " + rates[i]);

    document.getElementById("schedule").textContent = "";

    var was_hidden=document.getElementById("oracle").style.display == "none";
    if(was_hidden)
        document.getElementById("oracle").style.display = "initial";

    G = new Garden(rates, "garden");
    var e = document.getElementById("oracle_view");
    e.textContent = "";
    if(document.getElementById("reducemax").checked)
    {
        O = new ReduceMaxOracle(G.bamboos);
        D = new ReduceMaxDrawer(O, e);
    }
    else if(document.getElementById("reducefastest").checked)
    {
        var x = parseFloat(document.getElementById("reducefastest-x").value);
        O = new ReduceFastestOracle(G.bamboos, x);
        D = new ReduceFastestOracleDrawer(O, e);
    }
    else
    {
        O = new Oracle(G.bamboos);
        D = new TreeDrawer(O, e);
    }

    G.grow();
    G.draw();
    
    if(D!=null)
        D.draw();

    schedule = [];

    if(was_hidden)  
        document.getElementById("oracle").scrollIntoView();
}

function query() 
{
    var b = O.query();
    G.cut((b!=null)?b.tag:null);
        
    G.grow();
    G.draw();
    
    if(D!=null)
        D.draw();


    schedule.push((b!=null)?("b"+(b.tag+1)):"-");
    if(schedule.length>100)
        schedule.shift();
    
    document.getElementById("schedule").textContent = schedule.join(", ");
}

playing = false;
function play_next()
{
    if(!playing)
        return;

    query();
    setTimeout( play_next, 500 );
}


document.getElementById("build").addEventListener("click", () => { playing=false; build(); });
document.getElementById("query").addEventListener("click", () => { if(!playing) query(); });
document.getElementById("play").addEventListener("click", () => { playing = !playing; if(playing) play_next(); });


document.getElementById("reducefastest-x").addEventListener("change", () => { document.getElementById("reducefastest").checked = true; });


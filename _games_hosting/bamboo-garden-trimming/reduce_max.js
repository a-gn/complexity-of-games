class FakeUE
{
    constructor()
    {
        this.lines = [];
    }
    
    add_line(m, c, id)
    {
        this.lines.push( {m: m, c: c, id: id} );
    }
    
    delete_line(id)
    {
        for(var i=0; i<this.lines.length; i++)
        {
            if(this.lines[i].id == id)
            {
                this.lines.splice(i, 1);
                return;
            }
        }
    }
    
    query(x)
    {
        var maxval=null;
        var id=null;
        for(var i=0; i<this.lines.length; i++)
        {
            if(maxval==null || this.lines[i].m*x + this.lines[i].c > maxval)
            {
                maxval = this.lines[i].m*x + this.lines[i].c;
                id = this.lines[i].id;
            }
        }
        
        return id;
    }
    
    get_line(id)
    {
        for(var i=0; i<this.lines.length; i++)
            if(this.lines[i].id==id)
                return this.lines[i];
        
        return null;
    }
}

class ReduceMaxOracle
{
    constructor(bamboos) 
    {
        this.bamboos = [...bamboos];
        this.bamboos.sort((x, y) => { return y.rate - x.rate; });
        
        this.n = this.bamboos.length;
        this.day = 0;
        
        this.odd = true; //For drawing UE labels only
        this.ue1 = new FakeUE();
        this.ue2 = new FakeUE();
        
        this.first_loop = true;
    }
    
    update(pst, id, c)
    {
        pst.delete_line(id);
        pst.add_line(this.bamboos[id].rate, c, id);
    }
    
    query()
    {
        if(this.first_loop)
            this.ue1.add_line(this.bamboos[this.day].rate, this.bamboos[this.day].rate, this.day);
    
        var i = this.ue1.query(this.day);
        this.update(this.ue1, i, -this.day*this.bamboos[i].rate);
        this.update(this.ue2, i, (this.n - this.day)*this.bamboos[i].rate);
 
        var l = this.ue1.get_line(this.day);
        this.update(this.ue2, this.day, this.n * this.bamboos[this.day].rate + l.c);
        
        this.day = (this.day+1)%this.n;
        if(this.day==0)
        {
            var t = this.ue2;
            this.ue2 = this.ue1;
            this.ue1 = t;
            this.odd = !this.odd;
            this.first_loop = false;
        }
        
        return this.bamboos[i];
    }
}


class ReduceMaxDrawer
{
    constructor(oracle, element)
    {
        this.oracle = oracle;
        this.ue_width = 5 + (this.oracle.n-1)*20;
        this.ue_gap = 15;
        this.xmargin = 30;

        this.zero = 16;
        this.width = this.xmargin + 2*this.ue_width + this.ue_gap + 10;
        this.hmax =  this.oracle.bamboos.reduce((total, current) => total + current.rate, 0) * 5; 
        this.height = this.zero + this.hmax + 30;

        var canvas = document.createElement('canvas');
        canvas.style.width  = this.width.toString() + "px";
        canvas.style.height = this.height.toString() + "px";
        var dpr = window.devicePixelRatio || 1;
        canvas.width = this.width * dpr;
        canvas.height = this.height * dpr;
        this.ctx = canvas.getContext("2d");
        this.ctx.scale(dpr, dpr);
        element.appendChild(canvas);

        this.ctx.transform(1, 0, 0, -1, this.xmargin, this.height - this.zero);
        
        //this.ctx.imageSmoothingEnabled = false;
    }
    
    line(x0, y0, x1, y1, color, thickness)
    {
        this.ctx.strokeStyle = this.ctx.fillStyle  = color;
        this.ctx.lineWidth = thickness;
        this.ctx.beginPath();
        this.ctx.moveTo(x0, y0);
        this.ctx.lineTo(x1, y1);
        this.ctx.stroke();  
    }
    
    text(s, x, y, font, color, align, baseline)
    {
        this.ctx.font = font;
        this.ctx.strokeStyle = this.ctx.fillStyle  = color;
        this.ctx.textAlign = align;
        this.ctx.textBaseline = baseline;

        this.ctx.save();
        this.ctx.scale(1, -1);
        this.ctx.fillText(s, x, -y);
        this.ctx.restore();
    }
    
    clear()
    {
        this.ctx.clearRect(-this.xmargin, -this.zero, this.width, this.height);
    }
    
    draw()
    {
        this.clear();
        
        var ue1 = this.oracle.odd?this.oracle.ue1:this.oracle.ue2;
        var ue2 = this.oracle.odd?this.oracle.ue2:this.oracle.ue1;
        
        //Lines from ue1
        for(var i=0; i<ue1.lines.length; i++)
        {
            var l = ue1.lines[i];
            var color = (this.oracle.odd || l.id<this.oracle.day)?"#000000":"#666666";
            
            this.line(0, l.c, this.ue_width, this.ue_width/20 * l.m + l.c, color, 1);
        }
        
        //Lines from ue2
        for(var i=0; i<ue2.lines.length; i++)
        {
            var l = ue2.lines[i];
            var color = (!this.oracle.odd || l.id<this.oracle.day)?"#000000":"#666666";
            
            this.line(this.ue_width + this.ue_gap, l.c, this.ue_width + this.ue_gap + this.ue_width, this.ue_width/20 * l.m + l.c, color, 1);
        }

        //Axes ue1
        this.line(0, 0, 0, this.hmax + 3, "#000000", 1.5);
        this.line(-3, 0, this.ue_width, 0, "#000000", 1.5);
                
        //Axes ue2
        this.line(this.ue_width + this.ue_gap, 0, this.ue_gap + this.ue_width, this.hmax + 3, "#000000", 1.5);
        this.line(this.ue_gap + this.ue_width -3, 0, this.ue_gap+ 2*this.ue_width, 0, "#000000", 1.5);

        //X tickers
        for(var i=0; i<this.oracle.n; i+=1)
        {
            this.line(i*20, 0, i*20, -3, "#000000", 1);
            this.line(this.ue_gap + this.ue_width + i*20, 0, this.ue_gap + this.ue_width + i*20, -3, "#000000", 1);
            this.text(i.toString(), i*20, -4, "12px sans-serif", "#000000", "center", "top")
            this.text(i.toString(), this.ue_gap + this.ue_width + i*20,-4, "12px sans-serif", "#000000", "center", "top")
        }
        
        //Y tickers
        for(var i=0; i<=this.hmax; i+=20)
        {
            this.line(-3, i, 0, i, "#000000");
            this.line(this.ue_gap + this.ue_width -3, i, this.ue_gap + this.ue_width, i, "#000000", 1);
            this.text(i, -5, i, "12px sans-serif", "#000000", "right", "middle", 1)
        }
        
        //Vertical "day" line
        var offset = this.oracle.odd?0:(this.ue_gap + this.ue_width);
        this.line(offset + this.oracle.day*20, 0, offset + this.oracle.day*20, this.hmax, "#DD0000", 2)

        
        this.text("A", this.ue_width/2, this.hmax+5, "18px sans-serif", "#000000", "center", "bottom");
        this.text("B", this.ue_gap + this.ue_width + this.ue_width/2, this.hmax+5, "18px sans-serif", "#000000", "center", "bottom");
    }
}

GadgetType = 
{
    NONE        : 'gadget_none',
    SEQUENCER   : 'gadget_sequencer',
    CHOICE_WIRE : 'gadget_choice_wire',
    DISPLACER   : 'gadget_displacer',
    AUTO_WIRE   : 'gadget_auto_wire',
    GOAL_WIRE   : 'gadget_goal_wire',
    GOAL_GEM   :  'gadget_goal_gem'
}

function Cell(x, y, gem, type)
{
	this.x = x
	this.y = y
	this.gem = gem
	this.type = type
}

Cell.prototype.toString = function()
{
	return this.x+"_"+this.y
}

function Builder()
{
	this.field = new Array()//buckets.Dictionary()
    this.gadgets = Array()
        
	this.seq_top=0 //next cell
	this.seq_bottom=0 //last used cell
	this.seq_next_gem='C'
}

Builder.prototype.pile = function(i, j, gems, type)
{
	for (var k = 0; k < gems.length; k++)
	{
        if(gems[k]!='.')
            this.field.push( new Cell(i-k, j, gems[k], type) )
		
		//this.field.set(c, gems[k])
	}
}
			
Builder.prototype.addWire = function(i, j, l, ischoice)
{
	//this.pile(i, j, 'AB')
	//this.pile(i, j+1, "AB..AB......AB........AB")
	
    var type = ischoice?GadgetType.CHOICE_WIRE:GadgetType.AUTO_WIRE
    
	for(var k=0; k<l; k++)
	{
		var h = k - k%2
		
		if(k%2==0 || k==l-1)
			this.pile(i-h, j+k, "AB......AB........AB", type)
		else
			this.pile(i-h, j+k, "AB..AB..AB....AB..AB......AB", type)
	}
}

Builder.prototype.addChoice = function(i, j)
{
	this.pile(i-4, j, "AB", GadgetType.CHOICE_WIRE) //Sequencer BBCAABCCAB BBAAB (or DDCCAACD )
	this.pile(i+1, j+1, "AABBAABB......AB......AB........AB", GadgetType.CHOICE_WIRE)
}

Builder.prototype.addAuto = function(i, j)
{
	this.pile(i, j, "AB", GadgetType.AUTO_WIRE) //Sequencer AB
	this.pile(i, j+1, "AB..AB......AB........AB", GadgetType.AUTO_WIRE)
}

Builder.prototype.addOddEvenDisplacer = function(i, j, l)
{
	for(var k=0; k<l; k++)
	{
		var h = (k>>1)*3 // k//2*3
		if(k%2 == 0 || k==l-1)
			this.pile(i-h, j+k, "AAB", GadgetType.DISPLACER) //Sequencer AAB
		else
			this.pile(i-h, j+k, "AAB...AAB", GadgetType.DISPLACER)
	}
}

Builder.prototype.seqTop = function(gems, type)
{
	this.pile(this.seq_top, 0, gems, type)
	this.seq_top -= gems.length
}

Builder.prototype.seqBottom = function(gems, type)
{
	this.pile(this.seq_bottom+gems.length, 0, gems, type)
	this.seq_bottom += gems.length
}

Builder.prototype.seqGap = function(times)
{
	for(var i=0; i<times; i++)
	{
		if(this.seq_next_gem=='C')
		{		
			this.seqTop('C', GadgetType.SEQUENCER)
			this.seqBottom('CC', GadgetType.SEQUENCER)
			this.seq_next_gem = 'D'
		}
		else
		{
			this.seqTop('D', GadgetType.SEQUENCER)
			this.seqBottom('DD', GadgetType.SEQUENCER)
			this.seq_next_gem = 'C'

		}
	}
}

Builder.prototype.addClauseToWire = function(i, j, k) //k is the 0-based clause index
{		
	col = k*6+6+j
	row = -(k*6+6+1) + i - 30 + 1
	
	this.pile(row, col, "AB", GadgetType.CHOICE_WIRE) 
	this.pile(row, col-1, "......AB", GadgetType.CHOICE_WIRE)
	this.pile(row, col-2, "......AB", GadgetType.CHOICE_WIRE)
}

Builder.prototype.addGoalWire = function(i, j, l, nc)
{
	for(var k=0; k<l; k++)
	{
		var h = (k/3>>0)
	
		var clause= (k>5 && k%6<3  && (k/6>>0)<=nc )
		if(clause)
			h+=2
		
		if(k%3==0)
		{
			this.pile(i-h, j+k, 'B', GadgetType.GOAL_WIRE)
		}
		else if(k%3==1)
		{
			this.pile(i-h, j+k, 'A', GadgetType.GOAL_WIRE)
		}
		else if(k%3==2)
		{
			this.pile(i-h, j+k, 'A.A', GadgetType.GOAL_WIRE)
		}
	}
}

Builder.prototype.add2FallBlock = function(i, j)
{ //sequencer AB
	this.pile(i, j, "AB", GadgetType.CHOICE_WIRE)
	this.pile(i, j+1, "AB", GadgetType.CHOICE_WIRE)
}



Builder.prototype.build = function(numvars, formula)
{
    console.log(formula)
	var numclauses = formula.length
	
	var len = 6+6*numclauses

	var displacer_height = 	25 + 9*numclauses
	var auto_wire_height = 33 + 6*numclauses 
		
	//               wires      odd-even-displacers   shredders     
	var min_fall = 0*numvars + 2*3*numvars + 6*9*numvars
	var max_fall = 18*numvars + 2*6*numvars + 12*9*numvars
	
	var gap = 18+ 2*6 + 12*9 + 2//max single fall + 2
		
	for(var i=0; i<numvars; i++)
	{	
		this.seqTop('BBCAABCCAAB', GadgetType.CHOICE_WIRE)
		this.addChoice(this.seq_bottom, 1)
		this.addWire(this.seq_bottom-8, 3, len, true)
        this.gadgets.push( { name: 'Choice wire for x' + (i+1), row: this.seq_bottom, type: GadgetType.CHOICE_WIRE } )

		for(var j=0; j<numclauses; j++)
		{
            if(formula[j].indexOf(i+1)!=-1) //variables start from 1
                this.addClauseToWire(this.seq_bottom-8, 3, j)
		}

		this.seqGap(2)
		this.seqTop('AB', GadgetType.CHOICE_WIRE)
		this.add2FallBlock(this.seq_bottom, 1)
		this.seqGap(1)
		this.seqTop('AB', GadgetType.CHOICE_WIRE)
		this.add2FallBlock(this.seq_bottom, 1)
				
		this.seqGap(Math.ceil(displacer_height/2))	
		this.seqTop('AAB', GadgetType.DISPLACER)
		this.addOddEvenDisplacer(this.seq_bottom, 1, len+4)
        this.gadgets.push( { name: 'Top displacer', row: this.seq_bottom, type: GadgetType.DISPLACER } )

		for(var j=0; j<9; j++)
		{
			this.seqGap(Math.ceil(auto_wire_height/2))
			this.seqTop('AB', GadgetType.AUTO_WIRE)
			this.addAuto(this.seq_bottom, 1)
			this.addWire(this.seq_bottom-2, 3, len+2, false)
            this.gadgets.push( { name: 'Automatic wire ('+(j+1)+')', row: this.seq_bottom, type: GadgetType.AUTO_WIRE } )
		}

		this.seqGap(Math.ceil(displacer_height/2))
		this.seqTop('AAB', GadgetType.DISPLACER)
		this.addOddEvenDisplacer(this.seq_bottom, 1, len+4)
        this.gadgets.push( { name: 'Bottom displacer', row: this.seq_bottom, type: GadgetType.DISPLACER } )
        
		this.seqGap( gap*(numvars-i-1)/2 ) //is even
	}
	
	this.seqGap(1)

    this.gadgets.unshift( { name: 'Goal wire', row: this.seq_top, type: GadgetType.GOAL_WIRE} )
	for(var k=0; k<=max_fall /*-min_fall*/; k+=6)
	{
        if(k==0)
        {
            this.addGoalWire(this.seq_top, 1, len+8, numclauses)
            this.goal_gem_location = [this.seq_top-(len+6)/3, len+9]
            this.pile(this.goal_gem_location[0], this.goal_gem_location[1], "A", GadgetType.GOAL_GEM)
        }
        else
            this.addGoalWire(this.seq_top-k, 1, len+4, numclauses)
	}
	
    var checkpoint=this.seq_top
    this.seqGap(max_fall<<1);
    
	this.pile(checkpoint - (this.seq_bottom-this.seq_top), 0, "A",  GadgetType.GOAL_WIRE)
	//this.seqTop('A')
}

Builder.prototype.getMatrix = function(fill)
{            
	minx= 1e10
	maxx=-1e10
	maxy=-1e10
	for(var i=0; i<this.field.length; i++)
	{
		if(this.field[i].x<minx)
			minx=this.field[i].x
		
		if(this.field[i].x>maxx)
			maxx=this.field[i].x
			
		if(this.field[i].y>maxy)
			maxy=this.field[i].y
	}
	
	var f = []
	for(var i=0; i<=maxx-minx; i++)
	{
		f[i]=[]
		for(var j=0; j<=maxy; j++)
			f[i][j]=new Cell(i, j, '.', GadgetType.NONE)
	}

	for(var i=0; i<this.field.length; i++)
	{
		var c = this.field[i]
		f[c.x-minx][c.y]=c //.gem
	}
	
    if(fill)
    {
        for(var j=1; j<=maxy; j++)
        {    
            var filler = j%2==0?['C','D']:['E','F']
            var next=0
            for(var i=0; i<=maxx-minx; i++) 
            {
                if(f[i][j].gem!='.')
                    continue
                
                f[i][j].gem=filler[next]
                next=(next+1)%2                
            }
        }

        var next='C'
        for(var i=maxx-minx; i>=0; i--) 
        {
            if(f[i][0].gem=='C')
                next='D'
            else if(f[i][0].gem=='D')
                next='C'
            else if(f[i][0].gem=='.')
            {
                f[i][0].gem=next
                next=(next=='C'?'D':'C')
            }
        }
    }
    
    //FIXME
    for(var i=0; i<this.gadgets.length; i++)
	{
        this.gadgets[i].row-=minx
    }
    this.goal_gem_location[0]-=minx
    
	return f;
}
